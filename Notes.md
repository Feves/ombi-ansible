Notes

Infrastructure needed
EC2 
Static IP
S3 Bucket*2 (State&AppData)
VPC
VPN gateway

Containers needed
Nginx/Lets encrypt
OMBI
Vaultwarden

Next steps
-Ansible to launch the docker - then multiple containers
-Configure NGINX and Lets encrypt for revers proxy
-Create terraform to create required EC2 Infra
-Configure Containers
-Test DR process
-Run Services(For a few weeks)
-Check utilization for potential cost savings
-define auto update process
-define monitoring
