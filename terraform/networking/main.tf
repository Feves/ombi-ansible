# Configure the AWS Provider
provider "aws" {
  region = "eu-west-2"
}

################# AWS CERT MANAGER && Route53  ########################

resource "aws_route53_zone" "main" {
  name = "fevre.uk"
}

/*resource "aws_route53_zone" "vaultwarden" {
  name = "vaultwarden.fevre.uk"
}

resource "aws_route53_record" "vaultwarden-ns" {
  zone_id = aws_route53_zone.main.zone_id
  name    = "vaultwarden"
  type    = "NS"
  ttl     = "30"
  records = aws_route53_zone.vaultwarden.name_servers
}

resource "aws_route53_record" "wildcard" {
  for_each = {
    for dvo in aws_acm_certificate.wildcard_cert.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }

  name            = each.value.name
  records         = [each.value.record]
  ttl             = 60
  type            = each.value.type
  zone_id         = aws_route53_zone.main.zone_id
  allow_overwrite = true
}


resource "aws_acm_certificate" "wildcard_cert" {
  domain_name       = "*.fevre.uk"
  validation_method = "DNS"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_acm_certificate_validation" "example" {
  certificate_arn         = aws_acm_certificate.wildcard_cert.arn
}*/

