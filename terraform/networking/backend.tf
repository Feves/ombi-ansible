terraform {
  backend "s3" {
    bucket         = "fevre.uk-state-bucket"
    dynamodb_table = "fevre.uk-state-bucket-locks"
    encrypt        = true
    key            = "ombi/terraform/networking/terraform.tfstate"
    region         = "eu-west-2"
  }
}