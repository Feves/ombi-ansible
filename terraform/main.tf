terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = "eu-west-2"
}

########################## Instance ########################################

resource "aws_lightsail_instance" "ombi_lightsail" {
  name              = "ombi_lightsail_example"
  availability_zone = "eu-west-2b"
  blueprint_id      = "amazon_linux_2"
  bundle_id         = "micro_2_0"
  key_pair_name     = "lightsail"
  tags = {
    terraform = "true"
  }
  user_data          =<<EOF
    echo "yum update -y"
    yum -y update
    echo "yum install python3 python3-pip git -y"
    yum -y install python3 python3-pip git 
    echo "python3 -m pip install ansible docker docker-compose"
    python3 -m pip install ansible docker docker-compose
    echo "ansible-pull -U https://gitlab.com/Feves/ombi-ansible.git"
    ansible-pull -U https://gitlab.com/Feves/ombi-ansible.git
    EOF
}

###################################Networking#################################

resource "aws_lightsail_instance_public_ports" "Public_ports" {
  instance_name = aws_lightsail_instance.ombi_lightsail.name

  port_info {
    protocol  = "tcp"
    from_port = 22
    to_port   = 22
  }
    port_info {
    protocol  = "tcp"
    from_port = 3579
    to_port   = 3579
  }

  port_info {
    protocol  = "tcp"
    from_port = 82
    to_port   = 82
  }

  port_info {
    protocol  = "tcp"
    from_port = 81
    to_port   = 81
  }

  
  port_info {
    protocol  = "tcp"
    from_port = 8080
    to_port   = 8080
  }
}