output "public_ip_address" {
  value       = aws_lightsail_instance.ombi_lightsail.public_ip_address
  description = "The Ip Address name of the Lightsail instance."
}

output "created_at" {
  value       = aws_lightsail_instance.ombi_lightsail.created_at
  description = "The timestamp when the instance was created."
}

output "id" {
  value       = aws_lightsail_instance.ombi_lightsail.id
  description = "The ARN of the Lightsail instance."
}
